// Mini project
// Due 12nn Jun 27
// Javascript/jQuery game
// 1. Interactive to theuser
// 2. Appealing to the eye
// 3. Hosted in Gitlab
// Forlder name: JSProject


// visibility - change answer

// SET VARIABLES
let correctAnswer = [];
let submitChecking = false;
let chances = 0;


// SELECT COLOR
const selectRed = document.getElementById("red");
const selectBlue = document.getElementById("blue");
const selectGreen = document.getElementById("green");
const selectYellow = document.getElementById("yellow");
const selectViolet = document.getElementById("violet");
const selectBlack = document.getElementById("black");

// YOUR ANSWER
const answerList = document.getElementById("answerList");
let userAnswer = ["grey","grey","grey","grey"];
const answer = document.getElementsByClassName("answer");
answer[0].style.backgroundColor = "grey";
answer[1].style.backgroundColor = "grey";
answer[2].style.backgroundColor = "grey";
answer[3].style.backgroundColor = "grey";

// SUBMIT
const submit = document.getElementById("submit");
const clear = document.getElementById("clear");
const counter = document.getElementById("counter");
const newGame = document.getElementById("newGame");
const myModal = document.getElementById("myModal");



// FINAL ANSWER
const finalAnswer = document.getElementsByClassName("finalAnswer");
finalAnswer[0].style.backgroundColor = "grey";
finalAnswer[1].style.backgroundColor = "grey";
finalAnswer[2].style.backgroundColor = "grey";
finalAnswer[3].style.backgroundColor = "grey";

// console.log(finalAnswer);

// RANDOM CODE SETTING
// for(i = 0; i < 4; i++){
// 	correctAnswer[i] = Math.floor(Math.random() * 6) +1;
// }

// // let check = 0;
// for(i = 0; i < 4; i++){
// 	for(j = i+1; j < 4; j++){
// 		while(correctAnswer[i] == correctAnswer[j]) {
// 			correctAnswer[j] = Math.floor(Math.random() * 6) +1;
// 		}
// 	}
// }
let random = 0;
correctAnswer[0] = Math.floor(Math.random() * 6) +1;
for(i = 1; i < 4; i++){
	do{
		random = Math.floor(Math.random() * 6) +1;
	} while (correctAnswer.includes(random));
	correctAnswer[i] = random;
}

// correctAnswer.includes(userAnswer[i])

// $(document).ready(function () {
//     $('#div').html(Math.floor(Math.random() * 6) + 1);

//     var arr = [];
//     for (var i = 0; i < 5; i++)
//     {
//         arr.push(Math.floor(Math.random() * 6) + 1)
//     }
//     console.log(arr);
// });

console.log(correctAnswer);

// EVENTS
selectRed.addEventListener("click", function(){
	for(count=0; count<4 ; count++){
		if(answer[count].style.backgroundColor == "grey"){
			answer[count].style.backgroundColor = "red";
			userAnswer[count] = "red";
			return;
		}
	}
})

selectBlue.addEventListener("click", function(){
	for(count=0; count<4 ; count++){
		if(answer[count].style.backgroundColor == "grey"){
			answer[count].style.backgroundColor = "blue";
			userAnswer[count] = "blue";
			return;
		}
	}
})

selectGreen.addEventListener("click", function(){
	for(count=0; count<4 ; count++){
		if(answer[count].style.backgroundColor == "grey"){
			answer[count].style.backgroundColor = "green";
			userAnswer[count] = "green";
			return;
		}
	}
})

selectYellow.addEventListener("click", function(){
	for(count=0; count<4 ; count++){
		if(answer[count].style.backgroundColor == "grey"){
			answer[count].style.backgroundColor = "yellow";
			userAnswer[count] = "yellow";
			return;
		}
	}
})

selectViolet.addEventListener("click", function(){
	for(count=0; count<4 ; count++){
		if(answer[count].style.backgroundColor == "grey"){
			answer[count].style.backgroundColor = "violet";
			userAnswer[count] = "violet";
			return;
		}
	}
})

selectBlack.addEventListener("click", function(){
	for(count=0; count<4 ; count++){
		if(answer[count].style.backgroundColor == "grey"){
			answer[count].style.backgroundColor = "black";
			userAnswer[count] = "black";
			return;
		}
	}
})

clear.addEventListener("click", function(){
	for(i = 0; i < 4; i++){
		answer[i].style.backgroundColor = "grey";
		userAnswer[i] = "grey";
	}
})

answer[0].addEventListener("click",function(){
	answer[0].style.backgroundColor = "grey";
	userAnswer[0] = "grey";
})
answer[1].addEventListener("click",function(){
	answer[1].style.backgroundColor = "grey";
	userAnswer[1] = "grey";
})
answer[2].addEventListener("click",function(){
	answer[2].style.backgroundColor = "grey";
	userAnswer[2] = "grey";
})
answer[3].addEventListener("click",function(){
	answer[3].style.backgroundColor = "grey";
	userAnswer[3] = "grey";
})

newGame.addEventListener("click", function(){
	location.replace("miniproj.html");
})

submit.addEventListener("click", function(){
	
	// ERROR CHECKING

	if (userAnswer.includes("grey")) {
		alert("Error! Do not leave blank circle.");
		return;
	} else {
		for(i = 0; i < 4; i++){
			for(j = i+1; j < 4; j++){
				if (userAnswer[i] == userAnswer[j]){
					alert("Colors should be distinct");
					submitChecking.value = true;
					return;
				}
			}
		}


		if (submitChecking == true) {
			return;

		} else{


		const newAnswer = document.createElement("div");
		// console.log(newAnswer);
		for(count=0; count<4; count++){
			const newDiv = document.createElement("div");
			newDiv.classList.add("answer");

			newDiv.style.backgroundColor = answer[count].style.backgroundColor;
			// console.log(answer[count].style.backgroundColor);
			newAnswer.append(newDiv);

			// ASSIGN NUMBERS TO COLORS
			switch(answer[count].style.backgroundColor){
			case "red":
				userAnswer[count] = 1;
				break;
			case "blue":
				userAnswer[count] = 2;
				break;
			case "green":
				userAnswer[count] = 3;
				break;
			case "yellow":
				userAnswer[count] = 4;
				break;
			case "violet":
				userAnswer[count] = 5;
				break;
			case "black":
				userAnswer[count] = 6;
				break;
			case "grey":
				userAnswer[count] = 0;
				break;
			}

		}

			answerList.append(newAnswer);

			//CHECKING OF COLORS AND POSITION

			let checkCorrectColor = 0;
			let checkCorrectPosition = 0;

			for(i = 0; i < 4; i++){
				//CHECK NUMBER OF CORRECT COLORS
				if (correctAnswer.includes(userAnswer[i])){
					checkCorrectColor++;
				}
				
				//CHECK NUMBER OF CORRECT COLORS AND POSITION
				if (userAnswer[i] == correctAnswer[i]) {
					checkCorrectPosition++;
				}
			}
			// console.log(checkCorrectColor);
			// console.log(checkCorrectPosition);

			const correctColor = document.createElement("label");
			const correctPosition = document.createElement("label");
			// correctColor.innerHTML = "Correct Color: " + checkCorrectColor;
			// correctPosition.innerHTML = " ,   Correct Position " + checkCorrectPosition;

			correctColor.innerHTML = "Correct Color: " + checkCorrectColor + "<br>   Correct Position: " + checkCorrectPosition;
			// correctColor.setAttribute("style", "padding-bottom: 20px");

			newAnswer.append(correctColor);
			newAnswer.append(correctPosition);

			chances++;
			counter.innerHTML = 10 - chances;

			if(checkCorrectColor == 4 && checkCorrectPosition == 4){
				alert("Congratulations! You finished the game in " + chances + " trial/s.");
				// locstion.myModal.modal();

				for(i=0; i<4; i++){
					// console.log(correctAnswer[i]);
					switch(correctAnswer[i]){
					case 1:
						finalAnswer[i].style.backgroundColor = "red";
						break;
					case 2:
						finalAnswer[i].style.backgroundColor = "blue";
						break;
					case 3:
						finalAnswer[i].style.backgroundColor = "green";
						break;
					case 4:
						finalAnswer[i].style.backgroundColor = "yellow";
						break;
					case 5:
						finalAnswer[i].style.backgroundColor = "violet";
						break;
					case 6:
						finalAnswer[i].style.backgroundColor = "black";
						break;
					}
					
				}
				
			}

			for(i = 0; i < 4; i++){
				answer[i].style.backgroundColor = "grey";
				userAnswer[i] = "grey";
			}

			if (chances == 10 && (checkCorrectColor != 4 || checkCorrectPosition != 4)) {
				alert("Sorry. You already made 10 trials.");
				// location.replace("miniproj.html");
				for(i=0; i<4; i++){
					switch(correctAnswer[i]){
					case 1:
						finalAnswer[i].style.backgroundColor = "red";
						break;
					case 2:
						finalAnswer[i].style.backgroundColor = "blue";
						break;
					case 3:
						finalAnswer[i].style.backgroundColor = "green";
						break;
					case 4:
						finalAnswer[i].style.backgroundColor = "yellow";
						break;
					case 5:
						finalAnswer[i].style.backgroundColor = "violet";
						break;
					case 6:
						finalAnswer[i].style.backgroundColor = "black";
						break;
					}
					
				}

				// const submit = document.getElementById("submit");
				// const clear = document.getElementById("clear");

				submit.setAttribute("disabled", "true");
				clear.setAttribute("disabled", "true");

			}

		}
	}
})


/*
// ADD TO THE LIST OF ANSWERS
submit.addEventListener("click", function(){
	console.log(userAnswer);
	console.log(submitChecking==true);

	//ERROR CHECKING
	if (userAnswer.includes("grey")) {
		alert("Error! Do not leave blank circle.");
		return;
	} else {
		for(i = 0; i < 4; i++){
			for(j = i+1; j < 4; j++){
				if (userAnswer[i] == userAnswer[j]){
					alert("Colors should be distinct");
					submitChecking.value = true;
					console.log(submitChecking==true);
					return;
				}
			}
		}

		if (submitChecking == true) {
			return;
		}

	}


	// console.log(userAnswer);
	// console.log(submitChecking==true);

})
*/
